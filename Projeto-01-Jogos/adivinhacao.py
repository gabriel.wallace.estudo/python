print("*********************************")
print("Bem vindo ao jogo de adivinhação!")
print("*********************************")
print()

secret_number = 59
total = 3
round = 1

while (round <= total):
    print("Tentativa {} de {}".format(round, total))
    value = int(input("Digite o seu número: "))

    is_correct  = (value == secret_number)
    bigger      = (value > secret_number)

    if (is_correct):
        print("Você acertou!")
    else:
        if(bigger):
            print("Você errou! O seu chute foi maior que o número secreto")
        else:
            print("Você errou! O seu chute foi menor que o número secreto")

    round = round + 1

print("Fim de jogo")